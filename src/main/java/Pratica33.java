import utfpr.ct.dainf.if62c.pratica.Matriz;

/**
 * IF62C Fundamentos de Programação 2
 * Exemplo de programação em Java.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica33 {

    public static void main(String[] args) {
        Matriz m1 = new Matriz(3,3);
         
        m1.addValor(0, 0, 1.1);
        m1.addValor(0, 1, 1.2);
        m1.addValor(0, 2, 1.3);
        m1.addValor(1, 0, 2.1);
        m1.addValor(1, 1, 2.2);
        m1.addValor(1, 2, 2.3);
        m1.addValor(2, 0, 3.1);
        m1.addValor(2, 1, 3.2);
        m1.addValor(2, 2, 3.3);
        
        Matriz m2 = new Matriz(3,3);
        
        m2.addValor(0, 0, 1.1);
        m2.addValor(0, 1, 1.2);
        m2.addValor(0, 2, 1.3);
        m2.addValor(1, 0, 2.1);
        m2.addValor(1, 1, 2.2);
        m2.addValor(1, 2, 2.3);
        m2.addValor(2, 0, 3.1);
        m2.addValor(2, 1, 3.2);
        m2.addValor(2, 2, 3.3);
        
        Matriz matrizProd = m1.prod(m2);
        
        Matriz matrizSoma = m1.soma(m2);
        
        
        System.out.println("Matriz 1:\n" + m1);
        
        System.out.println("\nMatriz 2:\n" + m2);
        
        System.out.println("\nMatriz Soma:\n" + matrizSoma);
        
        System.out.println("\nMatriz Produto:\n" + matrizProd);
        
    }
}
